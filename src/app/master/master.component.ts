import { Component, OnInit } from '@angular/core';
import { NavigationStart, Router } from '@angular/router';
import { ReusableService } from '../services/api/reusable.service';
import { AppServiceService } from '../services/app-service.service';

@Component({
  selector: 'app-master',
  templateUrl: './master.component.html',
  styleUrls: ['./master.component.scss'],
})
export class MasterComponent implements OnInit {
  currentPage = 'home';
  constructor(
    private router: Router,
    private appService: AppServiceService,
    private reusableService: ReusableService
  ) {}

  ngOnInit(): void {
    this.configCurrentPage();
    this.eventRouter();
    this.setMenu();
  }
  setMenu() {
    this.reusableService.getMethod('/api/Menu/Getall').subscribe((res) => {
      this.appService.menu = res;
    });
  }
  goTo(link: string) {
    this.router.navigateByUrl(link);
  }
  eventRouter() {
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationStart) {
        this.currentPage = this.router.url.split('/')[2];
        this.appService.currentPage = this.currentPage;
        if (this.currentPage === 'keyword') {
          this.appService.activeLink =
            this.router.url.split('/')[3] === 'detail'
              ? 'list'
              : this.router.url.split('/')[3];
        } else {
          this.appService.activeLink = this.currentPage;
        }
      }
    });
  }
  configCurrentPage() {
    this.currentPage = this.router.url.split('/')[2];
    this.appService.currentPage = this.currentPage;
    if (this.currentPage === 'keyword') {
      this.appService.activeLink =
        this.router.url.split('/')[3] === 'detail'
          ? 'list'
          : this.router.url.split('/')[3];
    } else {
      this.appService.activeLink = this.currentPage;
    }
  }
}
