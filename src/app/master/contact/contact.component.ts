import { Component, OnInit } from '@angular/core';
interface contact {
  title: string;
  value: string;
  icon: string;
}
@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss'],
})
export class ContactComponent implements OnInit {
  contactList: contact[] = [
    {
      title: 'Địa chỉ',
      value:
        'Toà nhà GOBRANDING, 235 Lý Thường Kiệt, P. 6, Q. Tân Bình, Tp.HCM',
      icon: 'location-outline',
    },
    {
      title: 'Email',
      value: 'support@gobranding.com.vn',
      icon: 'mail-outline',
    },
    {
      title: 'Số điện thoại',
      value: '0913 713 679',
      icon: 'call-outline',
    },
  ];
  constructor() {}

  ngOnInit(): void {}
}
