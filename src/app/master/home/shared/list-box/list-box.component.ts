import { ListBox } from './../models/list-box.model';
import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'list-box',
  templateUrl: './list-box.component.html',
  styleUrls: ['./list-box.component.scss'],
})
export class ListBoxComponent implements OnInit {
  @Input() title!: string;
  @Input() searchPlaceholder!: string;
  @Input() listBox: ListBox[];
  @Input() fromKeyword: boolean = true;
  constructor(private router: Router) {}

  ngOnInit(): void {}
  goTo(to: 'keyword' | 'traffic', id: any) {
    this.router.navigate(['master', to, 'detail', id]);
  }
}
