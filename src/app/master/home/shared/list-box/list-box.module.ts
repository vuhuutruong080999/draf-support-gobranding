import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListBoxComponent } from './list-box.component';
import { MatInputModule } from '@angular/material/input';
@NgModule({
  declarations: [ListBoxComponent],
  imports: [CommonModule, MatInputModule, IonicModule],
  exports: [ListBoxComponent],
})
export class ListBoxModule {}
