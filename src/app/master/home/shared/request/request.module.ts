import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RequestComponent } from './request.component';
import { BoxItemModule } from '../box-item/box-item.module';

@NgModule({
  declarations: [RequestComponent],
  imports: [CommonModule, BoxItemModule],
  exports: [RequestComponent],
})
export class RequestModule {}
