import { Component, OnInit } from '@angular/core';
import { BoxItem } from '../models/box-item.model';

@Component({
  selector: 'home-request',
  templateUrl: './request.component.html',
  styleUrls: ['./request.component.scss'],
})
export class RequestComponent implements OnInit {
  requestBoxData: BoxItem[] = [
    { number: 2, type: 'Yêu cầu', title: 'mới gửi', color: 'blue' },
    { number: 36, type: 'Yêu cầu', title: 'đã tiếp nhận', color: 'green' },
    { number: 33, type: 'Yêu cầu', title: 'đang xử lý', color: 'yellow' },
    { number: 27, type: 'Yêu cầu', title: 'đã xử lý xong', color: 'red' },
    { number: 2, type: 'Yêu cầu', title: 'chờ phản hồi', color: 'blue' },
    { number: 36, type: 'Yêu cầu', title: 'đã đóng', color: 'green' },
  ];
  constructor() {}

  ngOnInit(): void {}
}
