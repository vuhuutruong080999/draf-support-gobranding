import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FilterTrafficComponent } from './filter-traffic.component';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

@NgModule({
  declarations: [FilterTrafficComponent],
  imports: [CommonModule, IonicModule, FormsModule],
})
export class FilterTrafficModule {}
