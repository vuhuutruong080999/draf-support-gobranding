import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';

@Component({
  selector: 'app-filter-traffic',
  templateUrl: './filter-traffic.component.html',
  styleUrls: ['./filter-traffic.component.scss'],
})
export class FilterTrafficComponent implements OnInit {
  to: string = '';
  from: string = '';
  constructor() {}

  ngOnInit(): void {}
  fromDatePicked(event: any) {
    this.from = moment(event.detail.value).format('DD/MM/yyyy');
  }
  toDatePicked(event: any) {
    this.to = moment(event.detail.value).format('DD/MM/yyyy');
  }
}
