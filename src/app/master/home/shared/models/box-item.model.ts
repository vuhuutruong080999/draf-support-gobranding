export interface BoxItem {
  number: number;
  type: string;
  title: string;
  color: 'blue' | 'green' | 'yellow' | 'red';
}
