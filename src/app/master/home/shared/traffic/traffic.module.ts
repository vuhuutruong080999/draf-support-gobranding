import { BoxItemModule } from './../box-item/box-item.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TrafficComponent } from './traffic.component';
import { IonicModule } from '@ionic/angular';
import { ListBoxModule } from '../list-box/list-box.module';
import { NgChartsModule } from 'ng2-charts';

@NgModule({
  declarations: [TrafficComponent],
  imports: [
    CommonModule,
    BoxItemModule,
    ListBoxModule,
    IonicModule,
    NgChartsModule,
  ],
  exports: [TrafficComponent],
})
export class TrafficModule {}
