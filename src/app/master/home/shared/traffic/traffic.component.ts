import { FilterTrafficComponent } from './../modals/filter-traffic/filter-traffic.component';
import { FilterModalComponent } from './../modals/filter-modal/filter-modal.component';
import { ListBox } from './../models/list-box.model';
import { BoxItem } from './../models/box-item.model';
import { Component, OnInit } from '@angular/core';
import { ChartConfiguration, ChartType } from 'chart.js';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'home-traffic',
  templateUrl: './traffic.component.html',
  styleUrls: ['./traffic.component.scss'],
})
export class TrafficComponent implements OnInit {
  trafficBoxData: BoxItem[] = [];
  listBox: ListBox[] = [];
  lineChartData: ChartConfiguration['data'] | undefined;
  lineChartOptions: ChartConfiguration['options'];
  lineChartType: ChartType = 'line';
  constructor(private modalController: ModalController) {}

  ngOnInit(): void {
    this.getBoxData();
    this.getListBoxData();
    this.getChart();
  }
  async filter() {
    const modal = await this.modalController.create({
      component: FilterTrafficComponent,
      initialBreakpoint: 0.37,
      breakpoints: [0, 0.5, 1],
      mode: 'ios',
    });
    return await modal.present();
  }
  getBoxData() {
    this.trafficBoxData = [
      { number: 2, type: 'Tổng số', title: 'website', color: 'blue' },
      { number: 36, type: 'Tổng Traffic', title: 'hôm nay', color: 'green' },
      { number: 33, type: 'Traffic', title: 'trung bình', color: 'yellow' },
      { number: 27, type: 'Tổng', title: 'Traffic', color: 'red' },
    ];
  }
  getListBoxData() {
    this.listBox = [
      {
        website: 'Gsoft.com.vn',
        updatedDate: '02/03/2022',
        listBoxNumber: [
          {
            number: 36,
            type: 'Traffic',
            title: 'trung bình',
          },
          {
            number: 36,
            type: 'Traffic',
            title: 'hôm qua',
          },
          {
            number: 36,
            type: 'Traffic',
            title: 'tốt nhất',
          },
        ],
      },
      {
        website: 'Gsoft.com.vn',
        updatedDate: '02/03/2022',
        listBoxNumber: [
          {
            number: 36,
            type: 'Traffic',
            title: 'trung bình',
          },
          {
            number: 36,
            type: 'Traffic',
            title: 'hôm qua',
          },
          {
            number: 36,
            type: 'Traffic',
            title: 'tốt nhất',
          },
        ],
      },
      {
        website: 'Gsoft.com.vn',
        updatedDate: '02/03/2022',
        listBoxNumber: [
          {
            number: 36,
            type: 'Traffic',
            title: 'trung bình',
          },
          {
            number: 36,
            type: 'Traffic',
            title: 'hôm qua',
          },
          {
            number: 36,
            type: 'Traffic',
            title: 'tốt nhất',
          },
        ],
      },
    ];
  }
  getChart() {
    this.lineChartData = {
      datasets: [
        {
          data: [65, 59, 200, 81, 56, 55, 40],
          backgroundColor: '#A1DDFF',
          borderColor: '#A1DDFF',
          pointBackgroundColor: '#A1DDFF',
          pointBorderColor: '#fff',
          fill: 'origin',
        },
      ],
      labels: ['22/02', '23/02', '24/02', '25/02', '26/02', '28/02', '29/02'],
    };
    this.lineChartOptions = {
      elements: {
        line: {
          tension: 0.5,
        },
      },
      scales: {
        x: {},
        'y-axis-0': {
          position: 'left',
          ticks: {
            stepSize: 25,
          },
          min: 0,
        },
      },
    };
  }
}
