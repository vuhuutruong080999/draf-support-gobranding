import { Component, Input, OnInit } from '@angular/core';
import { BoxItem } from '../models/box-item.model';

@Component({
  selector: 'box-item',
  templateUrl: './box-item.component.html',
  styleUrls: ['./box-item.component.scss'],
})
export class BoxItemComponent implements OnInit {
  @Input() boxItem!: BoxItem;
  constructor() {}

  ngOnInit(): void {}
}
