import { FilterModalComponent } from './../modals/filter-modal/filter-modal.component';
import { ModalController } from '@ionic/angular';
import { Component, OnInit, ViewChild } from '@angular/core';
import { BoxItem } from '../models/box-item.model';
import { ListBox } from '../models/list-box.model';
import { ChartConfiguration, ChartType } from 'chart.js';
import { BaseChartDirective } from 'ng2-charts';

@Component({
  selector: 'home-keyword',
  templateUrl: './keyword.component.html',
  styleUrls: ['./keyword.component.scss'],
})
export class KeywordComponent implements OnInit {
  keywordBoxData: BoxItem[] = [];
  listBox: ListBox[] = [];
  lineChartData: ChartConfiguration['data'] | undefined;
  lineChartOptions: ChartConfiguration['options'] | undefined;
  lineChartType: ChartType = 'line';
  @ViewChild(BaseChartDirective) chart?: BaseChartDirective;
  constructor(private modalController: ModalController) {}
  ngOnInit(): void {
    this.getboxData();
    this.getListBoxData();
    this.getChart();
  }
  async filter() {
    const modal = await this.modalController.create({
      component: FilterModalComponent,
      initialBreakpoint: 0.45,
      breakpoints: [0, 0.5, 1],
      mode: 'ios',
    });
    return await modal.present();
  }
  getboxData() {
    this.keywordBoxData = [
      { number: 2, type: 'Tổng số', title: 'website', color: 'blue' },
      { number: 36, type: 'Tổng số', title: 'từ khoá', color: 'green' },
      { number: 33, type: 'Từ khoá', title: 'đã lên top', color: 'yellow' },
      { number: 27, type: 'Từ khoá', title: 'lên top hiện tại', color: 'red' },
    ];
  }
  getListBoxData() {
    this.listBox = [
      {
        keyword: 'Phần mềm quản lý tài sản',
        website: 'Gsoft.com.vn',
        updatedDate: '02/03/2022',
        listBoxNumber: [
          {
            number: 36,
            type: 'Thứ hạng',
            title: 'tốt nhất',
          },
          {
            number: 36,
            type: 'Thứ hạng',
            title: 'hiện tại',
          },
          {
            number: 36,
            title: 'Biên độ',
            amplitude: -1,
          },
        ],
      },
      {
        keyword: 'Phần mềm quản lý tài sản',
        website: 'Gsoft.com.vn',
        updatedDate: '02/03/2022',
        listBoxNumber: [
          {
            number: 36,
            type: 'Thứ hạng',
            title: 'tốt nhất',
          },
          {
            number: 36,
            type: 'Thứ hạng',
            title: 'hiện tại',
          },
          {
            number: 36,
            title: 'Biên độ',
            amplitude: -1,
          },
        ],
      },
      {
        keyword: 'Phần mềm quản lý tài sản',
        website: 'Gsoft.com.vn',
        updatedDate: '02/03/2022',
        listBoxNumber: [
          {
            number: 36,
            type: 'Thứ hạng',
            title: 'tốt nhất',
          },
          {
            number: 36,
            type: 'Thứ hạng',
            title: 'hiện tại',
          },
          {
            number: 36,
            title: 'Biên độ',
            amplitude: 0,
          },
        ],
      },
      {
        keyword: 'Phần mềm quản lý tài sản',
        website: 'Gsoft.com.vn',
        updatedDate: '02/03/2022',
        listBoxNumber: [
          {
            number: 36,
            type: 'Thứ hạng',
            title: 'tốt nhất',
          },
          {
            number: 36,
            type: 'Thứ hạng',
            title: 'hiện tại',
          },
          {
            number: 36,
            title: 'Biên độ',
            amplitude: 4,
          },
        ],
      },
      {
        keyword: 'Phần mềm quản lý tài sản',
        website: 'Gsoft.com.vn',
        updatedDate: '02/03/2022',
        listBoxNumber: [
          {
            number: 36,
            type: 'Thứ hạng',
            title: 'tốt nhất',
          },
          {
            number: 36,
            type: 'Thứ hạng',
            title: 'hiện tại',
          },
          {
            number: 36,
            title: 'Biên độ',
            amplitude: -1,
          },
        ],
      },
      {
        keyword: 'Phần mềm quản lý tài sản',
        website: 'Gsoft.com.vn',
        updatedDate: '02/03/2022',
        listBoxNumber: [
          {
            number: 36,
            type: 'Thứ hạng',
            title: 'tốt nhất',
          },
          {
            number: 36,
            type: 'Thứ hạng',
            title: 'hiện tại',
          },
          {
            number: 36,
            title: 'Biên độ',
            amplitude: -1,
          },
        ],
      },
    ];
  }
  getChart() {
    this.lineChartData = {
      datasets: [
        {
          data: [65, 59, 200, 81, 56, 55, 40],
          backgroundColor: '#A1DDFF',
          borderColor: '#A1DDFF',
          pointBackgroundColor: '#A1DDFF',
          pointBorderColor: '#fff',
          fill: 'origin',
        },
      ],
      labels: ['22/02', '23/02', '24/02', '25/02', '26/02', '28/02', '29/02'],
    };
    this.lineChartOptions = {
      elements: {
        line: {
          tension: 0.5,
        },
      },
      scales: {
        x: {},
        'y-axis-0': {
          position: 'left',
          ticks: {
            stepSize: 25,
          },
          min: 0,
        },
      },
    };
  }
}
