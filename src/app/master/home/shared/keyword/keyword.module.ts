import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { KeywordComponent } from './keyword.component';
import { BoxItemModule } from '../box-item/box-item.module';
import { ListBoxModule } from '../list-box/list-box.module';
import { NgChartsModule } from 'ng2-charts';

@NgModule({
  declarations: [KeywordComponent],
  imports: [
    CommonModule,
    BoxItemModule,
    ListBoxModule,
    IonicModule,
    NgChartsModule,
  ],
  exports: [KeywordComponent],
})
export class KeywordModule {}
