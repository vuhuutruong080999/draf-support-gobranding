import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { FormsModule } from '@angular/forms';
import { KeywordModule } from './shared/keyword/keyword.module';
import { TrafficModule } from './shared/traffic/traffic.module';
import { RequestModule } from './shared/request/request.module';

@NgModule({
  declarations: [HomeComponent],
  imports: [
    CommonModule,
    HomeRoutingModule,
    IonicModule,
    FormsModule,
    KeywordModule,
    TrafficModule,
    RequestModule,
  ],
})
export class HomeModule {}
