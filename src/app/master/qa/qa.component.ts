import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-qa',
  templateUrl: './qa.component.html',
  styleUrls: ['./qa.component.scss'],
})
export class QaComponent implements OnInit {
  qaList = [
    'Hướng dẫn share quyền Google Analytics',
    'Hướng dẫn share quyền Google Webmaster Tools',
    'Hướng dãn kiểm tra bài viết copy',
    'Bài viết chuẩn SEO cho các từ khóa bán hàng',
    'Bài viết chuẩn SEO cho các từ khóa dịch vụ',
    'Hướng dẫn kiểm tra thứ hạng từ khóa trên Rankchecker',
  ];
  constructor() {}

  ngOnInit(): void {}
}
