import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TopRoutingModule } from './top-routing.module';
import { TopComponent } from './top.component';
import { ItemModule } from './shared/item/item.module';
import { MatInputModule } from '@angular/material/input';
import { IonicModule } from '@ionic/angular';

@NgModule({
  declarations: [TopComponent],
  imports: [
    CommonModule,
    TopRoutingModule,
    ItemModule,
    IonicModule,
    MatInputModule,
  ],
})
export class TopModule {}
