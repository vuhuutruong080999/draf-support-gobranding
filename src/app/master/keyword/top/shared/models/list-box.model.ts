export interface ListBoxNumber {
  number: number;
  type?: string;
}
export interface ListBox {
  keyword?: string;
  website: string;
  updatedDate?: string;
  dateTop: string;
  customer: string;
  listBoxNumber: ListBoxNumber;
}
