import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { FilterModalComponent } from './shared/filter-modal/filter-modal.component';
import { ListBox } from './shared/models/list-box.model';

@Component({
  selector: 'app-top',
  templateUrl: './top.component.html',
  styleUrls: ['./top.component.scss'],
})
export class TopComponent implements OnInit {
  listBox: ListBox[] = [];
  constructor(private modalController: ModalController) {}

  ngOnInit(): void {
    this.getListBoxData();
  }

  async filter() {
    const modal = await this.modalController.create({
      component: FilterModalComponent,
      initialBreakpoint: 0.5,
      breakpoints: [0, 0.5, 1],
      mode: 'ios',
    });
    return await modal.present();
  }
  getListBoxData() {
    this.listBox = [
      {
        keyword: 'Phần mềm quản lý tài sản',
        website: 'Gsoft.com.vn',
        updatedDate: '22/09/2000',
        dateTop: '08/09/1999',
        customer: 'Vũ Hữu Trường',
        listBoxNumber: {
          number: 36,
          type: 'Thứ hạng',
        },
      },
      {
        keyword: 'Phần mềm quản lý tài sản',
        website: 'Gsoft.com.vn',
        updatedDate: '22/09/2000',
        dateTop: '08/09/1999',
        customer: 'Vũ Hữu Trường',
        listBoxNumber: {
          number: 36,
          type: 'Thứ hạng',
        },
      },
      {
        keyword: 'Phần mềm quản lý tài sản',
        website: 'Gsoft.com.vn',
        updatedDate: '22/09/2000',
        dateTop: '08/09/1999',
        customer: 'Vũ Hữu Trường',
        listBoxNumber: {
          number: 36,
          type: 'Thứ hạng',
        },
      },
      {
        keyword: 'Phần mềm quản lý tài sản',
        website: 'Gsoft.com.vn',
        updatedDate: '22/09/2000',
        dateTop: '08/09/1999',
        customer: 'Vũ Hữu Trường',
        listBoxNumber: {
          number: 36,
          type: 'Thứ hạng',
        },
      },
      {
        keyword: 'Phần mềm quản lý tài sản',
        website: 'Gsoft.com.vn',
        updatedDate: '22/09/2000',
        dateTop: '08/09/1999',
        customer: 'Vũ Hữu Trường',
        listBoxNumber: {
          number: 36,
          type: 'Thứ hạng',
        },
      },
      {
        keyword: 'Phần mềm quản lý tài sản',
        website: 'Gsoft.com.vn',
        updatedDate: '22/09/2000',
        dateTop: '08/09/1999',
        customer: 'Vũ Hữu Trường',
        listBoxNumber: {
          number: 36,
          type: 'Thứ hạng',
        },
      },
      {
        keyword: 'Phần mềm quản lý tài sản',
        website: 'Gsoft.com.vn',
        updatedDate: '22/09/2000',
        dateTop: '08/09/1999',
        customer: 'Vũ Hữu Trường',
        listBoxNumber: {
          number: 36,
          type: 'Thứ hạng',
        },
      },
      {
        keyword: 'Phần mềm quản lý tài sản',
        website: 'Gsoft.com.vn',
        updatedDate: '22/09/2000',
        dateTop: '08/09/1999',
        customer: 'Vũ Hữu Trường',
        listBoxNumber: {
          number: 36,
          type: 'Thứ hạng',
        },
      },
      {
        keyword: 'Phần mềm quản lý tài sản',
        website: 'Gsoft.com.vn',
        updatedDate: '22/09/2000',
        dateTop: '08/09/1999',
        customer: 'Vũ Hữu Trường',
        listBoxNumber: {
          number: 36,
          type: 'Thứ hạng',
        },
      },
    ];
  }
}
