import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { KeywordRoutingModule } from './keyword-routing.module';

@NgModule({
  declarations: [],
  imports: [CommonModule, KeywordRoutingModule],
})
export class KeywordModule {}
