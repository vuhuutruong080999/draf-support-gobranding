import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListRoutingModule } from './list-routing.module';
import { ListComponent } from './list.component';
import { MatInputModule } from '@angular/material/input';
import { ItemModule } from './shared/item/item.module';

@NgModule({
  declarations: [ListComponent],
  imports: [CommonModule, ListRoutingModule, IonicModule, MatInputModule, ItemModule],
})
export class ListModule {}
