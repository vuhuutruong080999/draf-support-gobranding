import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { FilterModalComponent } from './shared/modals/filter-modal/filter-modal.component';
import { ListBox } from './shared/models/list-box.model';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class ListComponent implements OnInit {
  listBox: ListBox[] = [];
  constructor(private modalController: ModalController) {}

  ngOnInit(): void {
    this.getListBoxData();
  }
  async filter() {
    const modal = await this.modalController.create({
      component: FilterModalComponent,
      initialBreakpoint: 0.75,
      breakpoints: [0, 0.5, 1],
      mode: 'ios',
    });
    return await modal.present();
  }
  getListBoxData() {
    this.listBox = [
      {
        keyword: 'Phần mềm quản lý tài sản',
        website: 'Gsoft.com.vn',
        updatedDate: '02/03/2022',
        listBoxNumber: [
          {
            number: 36,
            type: 'Thứ hạng',
            title: 'tốt nhất',
          },
          {
            number: 36,
            type: 'Thứ hạng',
            title: 'hiện tại',
          },
          {
            number: 36,
            title: 'Biên độ',
            amplitude: -1,
          },
        ],
      },
      {
        keyword: 'Phần mềm quản lý tài sản',
        website: 'Gsoft.com.vn',
        updatedDate: '02/03/2022',
        listBoxNumber: [
          {
            number: 36,
            type: 'Thứ hạng',
            title: 'tốt nhất',
          },
          {
            number: 36,
            type: 'Thứ hạng',
            title: 'hiện tại',
          },
          {
            number: 36,
            title: 'Biên độ',
            amplitude: -1,
          },
        ],
      },
      {
        keyword: 'Phần mềm quản lý tài sản',
        website: 'Gsoft.com.vn',
        updatedDate: '02/03/2022',
        listBoxNumber: [
          {
            number: 36,
            type: 'Thứ hạng',
            title: 'tốt nhất',
          },
          {
            number: 36,
            type: 'Thứ hạng',
            title: 'hiện tại',
          },
          {
            number: 36,
            title: 'Biên độ',
            amplitude: 0,
          },
        ],
      },
      {
        keyword: 'Phần mềm quản lý tài sản',
        website: 'Gsoft.com.vn',
        updatedDate: '02/03/2022',
        listBoxNumber: [
          {
            number: 36,
            type: 'Thứ hạng',
            title: 'tốt nhất',
          },
          {
            number: 36,
            type: 'Thứ hạng',
            title: 'hiện tại',
          },
          {
            number: 36,
            title: 'Biên độ',
            amplitude: 4,
          },
        ],
      },
      {
        keyword: 'Phần mềm quản lý tài sản',
        website: 'Gsoft.com.vn',
        updatedDate: '02/03/2022',
        listBoxNumber: [
          {
            number: 36,
            type: 'Thứ hạng',
            title: 'tốt nhất',
          },
          {
            number: 36,
            type: 'Thứ hạng',
            title: 'hiện tại',
          },
          {
            number: 36,
            title: 'Biên độ',
            amplitude: -1,
          },
        ],
      },
      {
        keyword: 'Phần mềm quản lý tài sản',
        website: 'Gsoft.com.vn',
        updatedDate: '02/03/2022',
        listBoxNumber: [
          {
            number: 36,
            type: 'Thứ hạng',
            title: 'tốt nhất',
          },
          {
            number: 36,
            type: 'Thứ hạng',
            title: 'hiện tại',
          },
          {
            number: 36,
            title: 'Biên độ',
            amplitude: -1,
          },
        ],
      },
    ];
  }
}
