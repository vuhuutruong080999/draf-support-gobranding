import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReportRoutingModule } from './report-routing.module';
import { ReportComponent } from './report.component';
import { ItemModule } from './shared/item/item.module';
import { MatInputModule } from '@angular/material/input';

@NgModule({
  declarations: [ReportComponent],
  imports: [
    CommonModule,
    ReportRoutingModule,
    ItemModule,
    IonicModule,
    MatInputModule,
  ],
})
export class ReportModule {}
