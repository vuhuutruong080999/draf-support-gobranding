import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ChartConfiguration, ChartType } from 'chart.js';
import { FilterModalComponent } from '../modals/filter-modal/filter-modal.component';

@Component({
  selector: 'keyword-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.scss'],
})
export class ChartComponent implements OnInit {
  @Input() lineChartData: ChartConfiguration['data'] | undefined;
  @Input() lineChartOptions: ChartConfiguration['options'] | undefined;
  @Input() name: string | undefined;
  @Input() website: string | undefined;
  @Input() date: string | undefined;
  lineChartType: ChartType = 'line';
  constructor(private modalController: ModalController) {}

  ngOnInit(): void {}
  async filter() {
    const modal = await this.modalController.create({
      component: FilterModalComponent,
      initialBreakpoint: 0.3,
      breakpoints: [0, 0.5, 1],
      mode: 'ios',
    });
    return await modal.present();
  }
}
