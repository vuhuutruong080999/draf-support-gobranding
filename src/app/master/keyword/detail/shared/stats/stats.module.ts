import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StatsComponent } from './stats.component';
import { IonicModule } from '@ionic/angular';

@NgModule({
  declarations: [StatsComponent],
  imports: [CommonModule, IonicModule],
  exports: [StatsComponent],
})
export class StatsModule {}
