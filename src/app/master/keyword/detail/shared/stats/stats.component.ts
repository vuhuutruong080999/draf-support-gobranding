import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'keyword-stats',
  templateUrl: './stats.component.html',
  styleUrls: ['./stats.component.scss'],
})
export class StatsComponent implements OnInit {
  @Input() data!: any;
  constructor() {}

  ngOnInit(): void {}
}
