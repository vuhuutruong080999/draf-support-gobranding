import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'keyword-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.scss'],
})
export class InfoComponent implements OnInit {
  @Input() customerCode;
  @Input() landingPage;
  @Input() assigned;
  constructor() {}

  ngOnInit(): void {}
}
