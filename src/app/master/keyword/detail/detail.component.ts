import { Component, OnInit } from '@angular/core';
import { ChartConfiguration } from 'chart.js';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss'],
})
export class DetailComponent implements OnInit {
  lineChartData: ChartConfiguration['data'] | undefined;
  lineChartOptions: ChartConfiguration['options'] | undefined;
  keyword = {
    name: 'Phần mềm quản lý tài sản',
    website: 'Gsoft.com.vn',
    date: '04/03/2022',
    customerCode: 'GB002003',
    landingPage: 'https://gsoft.com.vn/quan-ly-mua-sam-2-2/',
    assigned: 'anhquan@gmail.com',
  };
  today = [
    { title: 'Thứ hạng', label: 'tốt nhất', number: 99 },
    { title: 'Thứ hạng', label: 'trung bình', number: 99 },
    { title: 'Thứ hạng', label: 'hôm qua', number: 99 },
    { title: 'Thứ hạng', label: 'hôm nay', number: 99 },
    { title: 'Thứ hạng', label: 'nhận về', number: 99 },
    { title: null, label: 'Biên độ', number: 99 },
  ];
  past = [
    { date: '03/12/2022', number: 99 },
    { date: '03/11/2022', number: 99 },
    { date: '03/10/2022', number: 99 },
    { date: '03/09/2022', number: 99 },
    { date: '03/08/2022', number: 99 },
    { date: '03/07/2022', number: 99 },
    { date: '03/06/2022', number: 99 },
    { date: '03/05/2022', number: 99 },
  ];
  show = false;
  constructor() {}

  ngOnInit(): void {
    this.getChart();
  }

  getChart() {
    this.lineChartData = {
      datasets: [
        {
          data: [65, 59, 200, 81, 56, 55, 40],
          backgroundColor: '#A1DDFF',
          borderColor: '#A1DDFF',
          pointBackgroundColor: '#A1DDFF',
          pointBorderColor: '#fff',
          fill: 'origin',
        },
      ],
      labels: ['22/02', '23/02', '24/02', '25/02', '26/02', '28/02', '29/02'],
    };
    this.lineChartOptions = {
      elements: {
        line: {
          tension: 0.5,
        },
      },
      scales: {
        x: {},
        'y-axis-0': {
          position: 'left',
          ticks: {
            stepSize: 25,
          },
          min: 0,
        },
      },
    };
  }
}
