import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { ViewWillEnter } from '@ionic/angular';
import { AppServiceService } from 'src/app/services/app-service.service';
interface menu {
  name: string;
  icon: string;
  routerLink: string;
  activeLink: string;
}
@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
})
export class MenuComponent implements OnInit, ViewWillEnter {
  listItem: menu[] = [];
  activeLink = '';
  constructor(private appService: AppServiceService, private router: Router) {}
  ionViewWillEnter(): void {
    this.config();
    this.activeLink = this.appService.activeLink;
  }

  ngOnInit(): void {}
  logout() {
    this.router.navigateByUrl('/auth/login');
  }
  config() {
    const list: menu[] = [
      {
        name: 'Trang chủ',
        icon: 'home-outline',
        routerLink: '/master/home',
        activeLink: 'home',
      },
      {
        name: 'Thông tin cá nhân',
        icon: 'person-circle-outline',
        routerLink: '/master/user',
        activeLink: 'user',
      },
      {
        name: 'Từ khoá',
        icon: 'lock-closed-outline',
        routerLink: '/master/keyword/list',
        activeLink: 'list',
      },
      {
        name: 'Từ khoá lên TOP lần đầu',
        icon: 'flame-outline',
        routerLink: '/master/keyword/top',
        activeLink: 'top',
      },
      {
        name: 'Báo cáo hạng từ khoá',
        icon: 'bar-chart-outline',
        routerLink: '/master/keyword/report',
        activeLink: 'report',
      },
      {
        name: 'Báo cáo Traffic',
        icon: 'pie-chart-outline',
        routerLink: '/master/traffic',
        activeLink: 'traffic',
      },
      {
        name: 'Tạo yêu cầu',
        icon: 'add-circle-outline',
        routerLink: '/master/request',
        activeLink: 'request',
      },
      {
        name: 'Hồ sơ tương tác KH',
        icon: 'people-circle-outline',
        routerLink: '/master/customer',
        activeLink: 'customer',
      },
      {
        name: 'Các câu hỏi thường gặp',
        icon: 'help-circle-outline',
        routerLink: '/master/QA',
        activeLink: 'QA',
      },
      {
        name: 'Liên hệ',
        icon: 'call-outline',
        routerLink: '/master/contact',
        activeLink: 'contact',
      },
    ];
    this.listItem = list.filter((res, i) => {
      if (this.appService.currentPage === 'home') {
        return res;
      } else if (this.appService.currentPage === 'keyword' && i !== 5) {
        return res;
      } else if (
        this.appService.currentPage === 'traffic' &&
        i !== 2 &&
        i !== 3 &&
        i !== 4
      ) {
        return res;
      } else {
        if (i !== 2 && i !== 3 && i !== 4 && i !== 5) {
          return res;
        }
      }
    });
  }
}
