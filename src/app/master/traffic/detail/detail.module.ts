import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DetailRoutingModule } from './detail-routing.module';
import { DetailComponent } from './detail.component';
import { ChartModule } from './shared/chart/chart.module';
import { StatsModule } from './shared/stats/stats.module';

@NgModule({
  declarations: [DetailComponent],
  imports: [
    CommonModule,
    DetailRoutingModule,
    ChartModule,
    StatsModule,
    IonicModule,
  ],
})
export class DetailModule {}
