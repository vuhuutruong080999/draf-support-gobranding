import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartComponent } from './chart.component';
import { IonicModule } from '@ionic/angular';
import { NgChartsModule } from 'ng2-charts';

@NgModule({
  declarations: [ChartComponent],
  imports: [CommonModule, IonicModule, NgChartsModule],
  exports: [ChartComponent],
})
export class ChartModule {}
