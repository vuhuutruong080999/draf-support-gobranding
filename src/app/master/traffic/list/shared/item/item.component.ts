import { Component, Input, OnInit } from '@angular/core';
import { ListBox } from '../models/list-box.model';

@Component({
  selector: 'list-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss'],
})
export class ItemComponent implements OnInit {
  @Input() item!: ListBox;
  constructor() {}

  ngOnInit(): void {}
}
