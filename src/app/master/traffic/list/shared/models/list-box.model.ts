export interface ListBoxNumber {
  number: number;
  type?: string;
  title: string;
  amplitude?: number;
}
export interface ListBox {
  keyword?: string;
  website: string;
  updatedDate?: string;
  listBoxNumber: ListBoxNumber[];
}
