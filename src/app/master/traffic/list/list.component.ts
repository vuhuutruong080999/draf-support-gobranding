import { Component, OnInit } from '@angular/core';
import { ListBox } from './shared/models/list-box.model';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class ListComponent implements OnInit {
  listBox: ListBox[] = [];
  constructor() {}

  ngOnInit(): void {
    this.getListBoxData();
  }

  getListBoxData() {
    this.listBox = [
      {
        website: 'Gsoft.com.vn',
        updatedDate: '02/03/2022',
        listBoxNumber: [
          {
            number: 36,
            type: 'Traffic',
            title: 'trung bình',
          },
          {
            number: 36,
            type: 'Traffic',
            title: 'hôm qua',
          },
          {
            number: 36,
            type: 'Traffic',
            title: 'tốt nhất',
          },
        ],
      },
      {
        website: 'Gsoft.com.vn',
        updatedDate: '02/03/2022',
        listBoxNumber: [
          {
            number: 36,
            type: 'Traffic',
            title: 'trung bình',
          },
          {
            number: 36,
            type: 'Traffic',
            title: 'hôm qua',
          },
          {
            number: 36,
            type: 'Traffic',
            title: 'tốt nhất',
          },
        ],
      },
      {
        website: 'Gsoft.com.vn',
        updatedDate: '02/03/2022',
        listBoxNumber: [
          {
            number: 36,
            type: 'Traffic',
            title: 'trung bình',
          },
          {
            number: 36,
            type: 'Traffic',
            title: 'hôm qua',
          },
          {
            number: 36,
            type: 'Traffic',
            title: 'tốt nhất',
          },
        ],
      },
      {
        website: 'Gsoft.com.vn',
        updatedDate: '02/03/2022',
        listBoxNumber: [
          {
            number: 36,
            type: 'Traffic',
            title: 'trung bình',
          },
          {
            number: 36,
            type: 'Traffic',
            title: 'hôm qua',
          },
          {
            number: 36,
            type: 'Traffic',
            title: 'tốt nhất',
          },
        ],
      },
      {
        website: 'Gsoft.com.vn',
        updatedDate: '02/03/2022',
        listBoxNumber: [
          {
            number: 36,
            type: 'Traffic',
            title: 'trung bình',
          },
          {
            number: 36,
            type: 'Traffic',
            title: 'hôm qua',
          },
          {
            number: 36,
            type: 'Traffic',
            title: 'tốt nhất',
          },
        ],
      },
      {
        website: 'Gsoft.com.vn',
        updatedDate: '02/03/2022',
        listBoxNumber: [
          {
            number: 36,
            type: 'Traffic',
            title: 'trung bình',
          },
          {
            number: 36,
            type: 'Traffic',
            title: 'hôm qua',
          },
          {
            number: 36,
            type: 'Traffic',
            title: 'tốt nhất',
          },
        ],
      },
    ];
  }
}
