import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListRoutingModule } from './list-routing.module';
import { ListComponent } from './list.component';
import { ItemModule } from './shared/item/item.module';

@NgModule({
  declarations: [ListComponent],
  imports: [CommonModule, ListRoutingModule, ItemModule, IonicModule],
})
export class ListModule {}
