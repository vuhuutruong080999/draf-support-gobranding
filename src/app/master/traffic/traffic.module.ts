import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TrafficRoutingModule } from './traffic-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    TrafficRoutingModule
  ]
})
export class TrafficModule { }
