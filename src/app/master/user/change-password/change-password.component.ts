import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss'],
})
export class ChangePasswordComponent implements OnInit {
  showOld = false;
  showNew = false;
  showConfirm = false;
  constructor(private router: Router) {}

  ngOnInit(): void {}
  onSubmit() {
    this.router.navigateByUrl('/master/user/info');
  }
}
