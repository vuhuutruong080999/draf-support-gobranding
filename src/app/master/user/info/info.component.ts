import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.scss'],
})
export class InfoComponent implements OnInit {
  user = {
    id: 'GB002003',
    name: 'Anh Quân',
    company: 'Công ty trách nhiệm hữu hạn Phần Mềm Hoàn Cầu GSoft',
    mail: 'anhquan@gmail.com',
    address: '235 Lý Thường Kiệt, Phường 6, Quận Tân Bình',
    taxCode: '0304932727',
    phone: '0123456789',
  };
  constructor() {}

  ngOnInit(): void {}
}
