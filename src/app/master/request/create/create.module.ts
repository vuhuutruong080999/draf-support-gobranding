import { FormControl } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CreateRoutingModule } from './create-routing.module';
import { CreateComponent } from './create.component';
import { ContentRequestModule } from './shared/content-request/content-request.module';
import { FileRequestModule } from './shared/file-request/file-request.module';

@NgModule({
  declarations: [CreateComponent],
  imports: [
    CommonModule,
    CreateRoutingModule,
    IonicModule,
    MatInputModule,
    ContentRequestModule,
    FileRequestModule,
  ],
})
export class CreateModule {}
