import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss'],
})
export class CreateComponent implements OnInit {
  service: string;
  website: string;
  department: string;
  request: string;
  title: string;
  content: string;
  constructor() {}

  ngOnInit(): void {}
}
