import { MatInputModule } from '@angular/material/input';
import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContentRequestComponent } from './content-request.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [ContentRequestComponent],
  imports: [CommonModule, IonicModule, MatInputModule, FormsModule],
  exports: [ContentRequestComponent],
})
export class ContentRequestModule {}
