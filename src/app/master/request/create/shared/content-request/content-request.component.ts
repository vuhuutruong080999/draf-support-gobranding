import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'content-request',
  templateUrl: './content-request.component.html',
  styleUrls: ['./content-request.component.scss'],
})
export class ContentRequestComponent implements OnInit {
  @Input() service: string = null;
  @Output() serviceChange = new EventEmitter<string>();
  @Input() website: string = null;
  @Output() websiteChange = new EventEmitter<string>();
  @Input() request: string = null;
  @Output() requestChange = new EventEmitter<string>();
  @Input() department: string = null;
  @Output() departmentChange = new EventEmitter<string>();
  @Input() title: string = null;
  @Output() titleChange = new EventEmitter<string>();
  @Input() content: string = null;
  @Output() contentChange = new EventEmitter<string>();
  constructor() {}

  ngOnInit(): void {}
  emitChangeService(event) {
    this.service = event.detail.value;
    this.serviceChange.emit(event.detail.value);
  }
  emitChangeWebsite(event) {
    this.websiteChange.emit(event.detail.value);
  }
  emitChangeRequest(event) {
    this.requestChange.emit(event.detail.value);
  }
  emitChangeDepartment(event) {
    this.departmentChange.emit(event.detail.value);
  }
  emitChangeTitle(event) {
    this.titleChange.emit(event.detail.value);
  }
  emitChangeContent(event) {
    this.contentChange.emit(event.detail.value);
  }
}
