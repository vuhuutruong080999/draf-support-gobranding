import { ToastController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'file-request',
  templateUrl: './file-request.component.html',
  styleUrls: ['./file-request.component.scss'],
})
export class FileRequestComponent implements OnInit {
  filesInput: File[] = [];
  constructor(
    private toastController: ToastController,
    private location: Location
  ) {}

  ngOnInit(): void {}

  clickFile(event) {
    event.target.value = '';
  }
  async selectFile(event) {
    const files: File[] = event.target.files;
    const maxFile = 25 * 1024 * 1024;
    let fileSize: File[] = [];
    let fileAdd = files;
    this.filesInput.forEach((res) => {
      fileAdd = fileAdd.filter((r) => r.name !== res.name);
    });
    fileSize = fileAdd.concat(this.filesInput);
    let size = 0;
    for (let index = 0; index < fileSize.length; index++) {
      const element = fileSize[index];
      size += element.size;
    }
    if (size <= maxFile) {
      this.filesInput = fileSize;
    }
  }
  async presentToast(message, duration) {
    const toast = await this.toastController.create({
      message: message,
      duration: duration,
      color: 'danger',
    });
    toast.present();
  }
  back() {
    this.location.back();
  }
}
