import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FileRequestComponent } from './file-request.component';

@NgModule({
  declarations: [FileRequestComponent],
  imports: [CommonModule, IonicModule],
  exports: [FileRequestComponent],
})
export class FileRequestModule {}
