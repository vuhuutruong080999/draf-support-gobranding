import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { FilterModalComponent } from './shared/modals/filter-modal/filter-modal.component';
import { ListBox } from './shared/models/list-box.model';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class ListComponent implements OnInit {
  stats = [
    { title: 'Yêu cầu', label: 'trung bình', number: 99 },
    { title: 'Yêu cầu', label: 'tốt nhất', number: 99 },
    { title: 'Yêu cầu', label: 'tốt nhất', number: 99 },
    { title: 'Yêu cầu', label: 'trung bình', number: 99 },
    { title: 'Chờ phản hồi', number: 99 },
    { title: 'Đã đóng', number: 99 },
    { title: 'Tất cả', number: 99 },
  ];
  listItem: ListBox[] = [];
  constructor(private modalController: ModalController) {}

  ngOnInit(): void {
    this.getListItem();
  }
  async filter() {
    const modal = await this.modalController.create({
      component: FilterModalComponent,
      initialBreakpoint: 0.5,
      breakpoints: [0, 0.5, 1],
      mode: 'ios',
    });
    return await modal.present();
  }
  getListItem() {
    this.listItem = [
      {
        request: 'Yêu cầu 1',
        website: 'Gsoft.com.vn',
        department: 'CNTT',
        id: 'GB0202020203040',
        type: 'Thực hiện dự án',
        status: 'Xử lý',
      },
      {
        request: 'Yêu cầu 1',
        website: 'Gsoft.com.vn',
        department: 'CNTT',
        id: 'GB0202020203040',
        type: 'Thực hiện dự án',
        status: 'Xử lý',
      },
      {
        request: 'Yêu cầu 1',
        website: 'Gsoft.com.vn',
        department: 'CNTT',
        id: 'GB0202020203040',
        type: 'Thực hiện dự án',
        status: 'Xử lý',
      },
      {
        request: 'Yêu cầu 1',
        website: 'Gsoft.com.vn',
        department: 'CNTT',
        id: 'GB0202020203040',
        type: 'Thực hiện dự án',
        status: 'Xử lý',
      },
      {
        request: 'Yêu cầu 1',
        website: 'Gsoft.com.vn',
        department: 'CNTT',
        id: 'GB0202020203040',
        type: 'Thực hiện dự án',
        status: 'Xử lý',
      },
      {
        request: 'Yêu cầu 1',
        website: 'Gsoft.com.vn',
        department: 'CNTT',
        id: 'GB0202020203040',
        type: 'Thực hiện dự án',
        status: 'Xử lý',
      },
      {
        request: 'Yêu cầu 1',
        website: 'Gsoft.com.vn',
        department: 'CNTT',
        id: 'GB0202020203040',
        type: 'Thực hiện dự án',
        status: 'Xử lý',
      },
    ];
  }
}
