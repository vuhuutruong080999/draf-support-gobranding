export interface ListBox {
  request?: string;
  website: string;
  department?: string;
  id: string;
  type: string;
  status: string;
}
