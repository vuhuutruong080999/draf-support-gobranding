import { Component, Input, OnInit } from '@angular/core';
import * as moment from 'moment';

@Component({
  selector: 'app-filter-modal',
  templateUrl: './filter-modal.component.html',
  styleUrls: ['./filter-modal.component.scss'],
})
export class FilterModalComponent implements OnInit {
  to: string = '';
  from: string = '';
  rank: { lower: number; upper: number } = { lower: 1, upper: 100 };
  constructor() {}

  ngOnInit(): void {}
  fromDatePicked(event: any) {
    this.from = moment(event.detail.value).format('DD/MM/yyyy');
  }
  toDatePicked(event: any) {
    this.to = moment(event.detail.value).format('DD/MM/yyyy');
  }
  changeRank(e) {
    this.rank = e.detail.value;
  }
}
