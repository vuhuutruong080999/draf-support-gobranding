import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FilterModalComponent } from './filter-modal.component';
import { FormsModule } from '@angular/forms';
@NgModule({
  declarations: [FilterModalComponent],
  imports: [CommonModule, IonicModule, FormsModule],
})
export class FilterModalModule {}
