import { Component, Input, OnInit } from '@angular/core';
import { ListBox } from '../models/list-box.model';

@Component({
  selector: 'report-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss'],
})
export class ItemComponent implements OnInit {
  @Input() itemDetail!: ListBox;
  constructor() {}

  ngOnInit(): void {}
}
