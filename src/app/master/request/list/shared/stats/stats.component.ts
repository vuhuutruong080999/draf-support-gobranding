import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'request-stats',
  templateUrl: './stats.component.html',
  styleUrls: ['./stats.component.scss'],
})
export class StatsComponent implements OnInit {
  @Input() data!: any;
  @Input() color: string;
  constructor() {}

  ngOnInit(): void {}
}
