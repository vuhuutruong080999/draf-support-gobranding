import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListRoutingModule } from './list-routing.module';
import { ListComponent } from './list.component';
import { StatsModule } from './shared/stats/stats.module';
import { ItemModule } from './shared/item/item.module';

@NgModule({
  declarations: [ListComponent],
  imports: [
    CommonModule,
    ListRoutingModule,
    IonicModule,
    StatsModule,
    ItemModule,
  ],
})
export class ListModule {}
