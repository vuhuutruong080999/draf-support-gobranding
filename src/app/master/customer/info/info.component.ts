import { Component, OnInit } from '@angular/core';
import { Info } from './shared/models/info.model';

@Component({
  selector: 'app-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.scss'],
})
export class InfoComponent implements OnInit {
  info: Info = {
    name: 'Công ty CP Global Online Branding',
    signatureAuthor: 'Phan Trung Hiếu',
    representative: 'Vũ Hữu Trường',
    phone: '0982498257',
    email: 'truongvh@ggroup.com.vn',
    supportContact: '08 9990 9139 x 199',
    supportTech: '08 9990 9139 x 193',
    supportCustomer: '08 9990 9139 x 199',
  };
  constructor() {}

  ngOnInit(): void {}
}
