export interface Info {
  name: string;
  signatureAuthor: string;
  representative: string;
  phone: string;
  email: string;
  supportContact: string;
  supportTech: string;
  supportCustomer: string;
}
