import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'customer-contact',
    loadChildren: () =>
      import('./customer-contact/customer-contact.module').then(
        (m) => m.CustomerContactModule
      ),
  },
  {
    path: 'interactive/:id',
    loadChildren: () =>
      import('./interactive/interactive.module').then(
        (m) => m.InteractiveModule
      ),
  },
  {
    path: 'info/:id',
    loadChildren: () => import('./info/info.module').then((m) => m.InfoModule),
  },
  { path: '', pathMatch: 'full', redirectTo: 'customer-contact' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CustomerRoutingModule {}
