export interface Contact {
  name: string;
  company: string;
  service: string;
  contractNumber: string;
}
