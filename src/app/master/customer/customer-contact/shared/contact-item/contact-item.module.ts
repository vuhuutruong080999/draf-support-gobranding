import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContactItemComponent } from './contact-item.component';

@NgModule({
  declarations: [ContactItemComponent],
  imports: [CommonModule, IonicModule],
  exports: [ContactItemComponent],
})
export class ContactItemModule {}
