import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CustomerContactRoutingModule } from './customer-contact-routing.module';
import { CustomerContactComponent } from './customer-contact.component';
import { ContactItemModule } from './shared/contact-item/contact-item.module';

@NgModule({
  declarations: [CustomerContactComponent],
  imports: [CommonModule, CustomerContactRoutingModule, IonicModule, ContactItemModule],
})
export class CustomerContactModule {}
