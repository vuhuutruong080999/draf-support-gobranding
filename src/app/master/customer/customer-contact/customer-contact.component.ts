import { Component, OnInit } from '@angular/core';
import { Contact } from './shared/models/contact.model';

@Component({
  selector: 'app-customer-contact',
  templateUrl: './customer-contact.component.html',
  styleUrls: ['./customer-contact.component.scss'],
})
export class CustomerContactComponent implements OnInit {
  contactList: Contact[] = [
    {
      name: 'Vũ Hữu Trường',
      company: 'Công ty CP Global Online Branding',
      service: 'gobrading.com.vn',
      contractNumber: '1400.07-A/HÐDV-GOBRANDING20',
    },
    {
      name: 'Vũ Hữu Trường',
      company: 'Công ty CP Global Online Branding',
      service: 'gobrading.com.vn',
      contractNumber: '1400.07-A/HÐDV-GOBRANDING20',
    },
    {
      name: 'Vũ Hữu Trường',
      company: 'Công ty CP Global Online Branding',
      service: 'gobrading.com.vn',
      contractNumber: '1400.07-A/HÐDV-GOBRANDING20',
    },
    {
      name: 'Vũ Hữu Trường',
      company: 'Công ty CP Global Online Branding',
      service: 'gobrading.com.vn',
      contractNumber: '1400.07-A/HÐDV-GOBRANDING20',
    },
    {
      name: 'Vũ Hữu Trường',
      company: 'Công ty CP Global Online Branding',
      service: 'gobrading.com.vn',
      contractNumber: '1400.07-A/HÐDV-GOBRANDING20',
    },
    {
      name: 'Vũ Hữu Trường',
      company: 'Công ty CP Global Online Branding',
      service: 'gobrading.com.vn',
      contractNumber: '1400.07-A/HÐDV-GOBRANDING20',
    },
    {
      name: 'Vũ Hữu Trường',
      company: 'Công ty CP Global Online Branding',
      service: 'gobrading.com.vn',
      contractNumber: '1400.07-A/HÐDV-GOBRANDING20',
    },
    {
      name: 'Vũ Hữu Trường',
      company: 'Công ty CP Global Online Branding',
      service: 'gobrading.com.vn',
      contractNumber: '1400.07-A/HÐDV-GOBRANDING20',
    },
  ];
  constructor() {}

  ngOnInit(): void {}
}
