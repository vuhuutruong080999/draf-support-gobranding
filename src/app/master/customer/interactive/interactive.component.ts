import { Interactive } from './shared/models/interactive.model';
import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { ActivatedRoute } from '@angular/router';
const content = `<b>GOBRANDING gửi Quý khách bộ hồ sơ thanh lý 1 phần dự án</b>
</br></br>
      Kính Gửi Quý khách,
</br></br>
      GOBRANDING đã nhận được phản hồi từ Quý khách, GOBRANDING gửi lại Quý khách bộ hồ sơ thanh lý 1 phần dự án, Quý khách vui lòng xem qua giúp(file đính kèm).
</br></br>
      Nếu không có vấn đề gì thêm, Quý khách vui lòng in hồ sơ ra 2 bộ (2 biên bản thanh lý, 2 biên bản nghiệm thu), ký, đóng dấu xác nhận của Quý khách và gửi về GOBRANDING giúp, để GOBRANDING tiến hành hoàn thiện hồ sơ thanh lý 1 phần dự án và sẽ gửi lại cho Quý khách 1 bản.
</br></br>
      Thông tin nhận hồ sơ là: Công ty CP Global Online Branding
</br></br>
      ​Địa chỉ: Tầng 6 - 7 - 8 toà nhà GOBRANDING, 235 Lý Thường Kiệt, P. 6, Q. Tân Bình, Tp.HCM
</br></br>
      Người nhận: Hà Linh
</br></br>
      ​Số điện thoại: 0090033456
</br></br>
      Mọi thông tin Quý khách vui lòng gửi phản hồi về cho GOBRANDING qua support.gobranding.com.vn hoặc liên lạc qua số điện thoại: 0964678657 để được hỗ trợ.Mong sớm nhận được phản hồi từ Quý khách.
      
</br></br>
      Cảm ơn Quý khách.
</br></br>
      Trân trọng!`;
@Component({
  selector: 'app-interactive',
  templateUrl: './interactive.component.html',
  styleUrls: ['./interactive.component.scss'],
})
export class InteractiveComponent implements OnInit {
  fileTxt = new File(['foo'], 'foo.txt', {
    type: 'text/plain',
  });
  fileDoc = new File(
    ['foo'],
    'application/vnd.openxmlformats-officedocument.wordprocessingml.document.docs',
    {
      type: 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
    }
  );
  interactiveList: Interactive[] = [
    {
      id: '1',
      name: 'Trường Vũ',
      createdTime: moment('09082022220900', 'MMDDYYYYHHmms').format(
        'HH:mm DD/MM'
      ),
      title: 'Yêu cầu gửi tài liệu SRD của dự án liên quan đến gobranding',
      content: content,
      files: [this.fileTxt, this.fileDoc],
    },
    {
      id: '2',
      name: 'Trường Vũ',
      createdTime: moment('09082022220900', 'MMDDYYYYHHmms').format(
        'HH:mm DD/MM'
      ),
      title: 'Yêu cầu gửi tài liệu SRD của dự án liên quan đến gobranding',
      content: content,
    },
    {
      id: '3',
      name: 'Trường Vũ',
      createdTime: moment('09082022220900', 'MMDDYYYYHHmms').format(
        'HH:mm DD/MM'
      ),
      title: 'Yêu cầu gửi tài liệu SRD của dự án liên quan đến gobranding',
      content: content,
    },
    {
      id: '4',
      name: 'Trường Vũ',
      createdTime: moment('09082022220900', 'MMDDYYYYHHmms').format(
        'HH:mm DD/MM'
      ),
      title: 'Yêu cầu gửi tài liệu SRD của dự án liên quan đến gobranding',
      content: content,
    },
    {
      id: '5',
      name: 'Trường Vũ',
      createdTime: moment('09082022220900', 'MMDDYYYYHHmms').format(
        'HH:mm DD/MM'
      ),
      title: 'Yêu cầu gửi tài liệu SRD của dự án liên quan đến gobranding',
      content: content,
    },
  ];
  opened = '';
  id: string = '';
  constructor(private route: ActivatedRoute) {}

  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get('email');
  }
  openItem(id: string) {
    if (this.opened !== id) {
      this.opened = id;
    } else {
      this.opened = '';
    }
  }
}
