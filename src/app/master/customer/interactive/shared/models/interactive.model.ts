export interface Interactive {
  id: string;
  name: string;
  createdTime: string;
  title: string;
  content: string;
  files?: File[];
}
