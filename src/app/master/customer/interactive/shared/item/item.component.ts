import { Interactive } from './../models/interactive.model';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'interactive-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss'],
})
export class ItemComponent implements OnInit {
  @Input() item: Interactive;
  @Input() opened: string = '';
  @Output() openedChange: EventEmitter<string> = new EventEmitter();
  constructor() {}

  ngOnInit(): void {}
  openItem() {
    if (this.opened !== this.item.id) {
      this.openedChange.emit(this.item.id);
    } else {
      this.openedChange.emit('');
    }
  }
}
