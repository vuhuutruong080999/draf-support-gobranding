import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InteractiveRoutingModule } from './interactive-routing.module';
import { InteractiveComponent } from './interactive.component';
import { ItemModule } from './shared/item/item.module';

@NgModule({
  declarations: [InteractiveComponent],
  imports: [CommonModule, InteractiveRoutingModule, IonicModule, ItemModule],
})
export class InteractiveModule {}
