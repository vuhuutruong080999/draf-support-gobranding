import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-confirm-email',
  templateUrl: './confirm-email.component.html',
  styleUrls: ['./confirm-email.component.scss'],
})
export class ConfirmEmailComponent implements OnInit {
  count: number = 60;
  setCount: any;
  joinTime: number = 0;
  constructor() {}

  ngOnInit(): void {
    this.resetTime();
  }
  resend() {
    this.resetTime();
  }
  resetTime() {
    this.joinTime = Date.now();
    this.count = 60;
    this.setCount = setInterval(() => {
      if (this.count > 0) {
        if (60000 - (Date.now() - this.joinTime) <= 0) {
          this.count = 0;
        } else {
          this.count = parseInt(
            ((60000 - (Date.now() - this.joinTime)) / 1000).toFixed(0)
          );
        }
      } else clearInterval(this.setCount);
    }, 1000);
  }
}
