import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ReusableService } from 'src/app/services/api/reusable.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  hidePassword = true;
  formLogin: FormGroup = this.fb.group({
    username: ['', [Validators.required]],
    password: ['', [Validators.required]],
  });
  constructor(
    private router: Router,
    private fb: FormBuilder,
    private reusableService: ReusableService
  ) {}

  ngOnInit(): void {}
  login() {
    // const payload = {
    //   username: this.formLogin.get('username').value,
    //   password: this.formLogin.get('password').value,
    // };
    // this.reusableService.login('/token', payload).subscribe(console.log);
    this.router.navigate(['/master/home']);
  }
}
