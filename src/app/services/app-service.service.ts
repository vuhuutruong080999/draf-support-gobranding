import { IMenu } from './../models/app-model.model';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class AppServiceService {
  private _currentPage: string = 'home';
  private _activeLink: string = 'home';
  private _menu: IMenu[];
  constructor() {}
  get currentPage(): string {
    return this._currentPage;
  }
  set currentPage(currentPage: string) {
    this._currentPage = currentPage;
  }
  get activeLink(): string {
    return this._activeLink;
  }
  set activeLink(activeLink: string) {
    this._activeLink = activeLink;
  }
  get menu(): IMenu[] {
    return this._menu;
  }
  set menu(menu: IMenu[]) {
    this._menu = menu;
  }
}
