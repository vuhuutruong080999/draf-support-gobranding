import { AppServiceService } from './services/app-service.service';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { HttpConfigInterceptor } from './core/http/httpconfig.interceptor';
import { AuthModule } from './auth/auth.module';
import { LoginModule } from './auth/login/login.module';
import { ForgotPasswordModule } from './auth/forgot-password/forgot-password.module';
import { ConfirmEmailModule } from './auth/confirm-email/confirm-email.module';
import { ResetPasswordModule } from './auth/reset-password/reset-password.module';
import { MasterModule } from './master/master.module';
import { HomeModule } from './master/home/home.module';
import { NgChartsModule } from 'ng2-charts';
import { KeywordModule } from './master/keyword/keyword.module';
import { ListModule } from './master/keyword/list/list.module';
import { DetailModule } from './master/keyword/detail/detail.module';
import { MenuModule } from './master/menu/menu.module';
import { UserModule } from './master/user/user.module';
import { InfoModule } from './master/user/info/info.module';
import { ChangePasswordModule } from './master/user/change-password/change-password.module';
import { QaModule } from './master/qa/qa.module';
import { ContactModule } from './master/contact/contact.module';
import { ReportModule } from './master/keyword/report/report.module';
import { TopModule } from './master/keyword/top/top.module';
import { TrafficModule } from './master/traffic/traffic.module';
import { RequestModule } from './master/request/request.module';
import { CreateModule } from './master/request/create/create.module';
import { CustomerModule } from './master/customer/customer.module';
import { CustomerContactModule } from './master/customer/customer-contact/customer-contact.module';
import { InteractiveModule } from './master/customer/interactive/interactive.module';
@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    HttpClientModule,
    AuthModule,
    LoginModule,
    ForgotPasswordModule,
    ConfirmEmailModule,
    ResetPasswordModule,
    MasterModule,
    HomeModule,
    NgChartsModule,
    KeywordModule,
    ListModule,
    DetailModule,
    MenuModule,
    UserModule,
    InfoModule,
    ChangePasswordModule,
    QaModule,
    ContactModule,
    ReportModule,
    TopModule,
    TrafficModule,
    RequestModule,
    CreateModule,
    CustomerModule,
    CustomerContactModule,
    InteractiveModule,
  ],
  providers: [
    AppServiceService,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpConfigInterceptor,
      multi: true,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
