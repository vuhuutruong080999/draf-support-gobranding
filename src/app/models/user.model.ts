import * as moment from 'moment';

interface IExportExcelButton {
  buttonName: string;
  isShow: boolean;
}
export interface IUser {
  address?: string;
  approveDT?: moment.Moment;
  authStatus?: string;
  brancH_ID?: string;
  chargeDT?: moment.Moment;
  checkID?: string;
  companyName?: string;
  contractID?: number | string;
  createDT?: moment.Moment;
  customerName?: string;
  dataTemp?: any;
  depID?: number;
  dxContactPerson?: string;
  dxSurrogate?: string;
  editorDT?: moment.Moment;
  editorID?: string;
  email?: string;
  emailCc?: string;
  emailSurrogate?: string;
  expContract?: string;
  hasRegistered?: boolean;
  listExportExcelButton?: IExportExcelButton[];
  loginProvider?: any;
  makerID?: string;
  notes?: string;
  passWord?: string;
  phoneNumber?: string;
  recordStatus?: string;
  roles?: string[];
  signContractDT?: moment.Moment;
  status?: string;
  surrogate?: string;
  taxCode?: string;
  userName?: string;
  value?: string;
  xmlTemp?: string;
}
