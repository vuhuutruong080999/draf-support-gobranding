interface ISettings {
  content: string;
  id: number;
  loginRequired: boolean;
  nav: number;
  parentID: number;
  roles: string[];
}
interface IConfig {
  isDeleteRequested?: boolean;
  isDetail?: boolean;
  isNew?: boolean;
  templateUrl: string;
  title: string;
  settings: ISettings;
}
export interface IMenu {
  chirldren: string;
  url: string;
  config: IConfig;
}
