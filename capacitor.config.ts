import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'support.gob.online',
  appName: 'Support GoBranding Online',
  webDir: 'www',
  bundledWebRuntime: false,
};

export default config;
